import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/sequelize';
import {UserModel} from './user.model';
import {LoginDto, UserEmailRequestDto, UserIdDto, UserRegisterDto} from './user.request.dto';
import * as bcrypt from 'bcrypt'
import * as jwt from 'jsonwebtoken'
import {apiMessagesEnum} from "../../libs/enum/src";
import {audit} from "rxjs/operators";
import {TasksModel} from "../tasks/tasks.model";

@Injectable()
export class UserService {
    constructor(
        @InjectModel(UserModel) private userRepository: typeof UserModel,
    ) {
    }

    async create(registerDto: UserRegisterDto) {
        const user = await this.userRepository.findOne({
            where: {
                email: registerDto.email
            }
        });
        if (user) {
            throw new HttpException(apiMessagesEnum.emailInUse, HttpStatus.BAD_REQUEST);
        }
        const createdUser = new this.userRepository(registerDto)
        await createdUser.save()
        return this.sanitizeUser(createdUser)
    }

    async getAllUsers() {
        return this.userRepository.findAll()
    }

    async findByEmail(request: UserEmailRequestDto) {
        return this.userRepository.findAll({
            where: {
                email: request.email
            }
        })
    }


    async deactivateUser(request: UserEmailRequestDto, token) {

        const decodedRole = await jwt.verify(token, process.env.SECRET_KEY,
            function (err, decoded) {
                return decoded.role;
            })

        console.log(decodedRole)


        return null;
    }

    async promoteUser(request: UserEmailRequestDto, token) {

        const decodedRole = await jwt.verify(token, process.env.SECRET_KEY,
            function (err, decoded) {
                return decoded.role;
            })
        console.log('decodedRole "' + decodedRole + '"')
        console.log(decodedRole)
        if(`${decodedRole}` !== 'admin'){
            throw new HttpException(apiMessagesEnum.unauthorized, HttpStatus.FORBIDDEN)
        }

        // await this.checkIfAdmin(token)

        const userToUpdate: UserModel[] = await this.findByEmail(request);
        if (!userToUpdate) {
            throw new HttpException(apiMessagesEnum.userNotFound, HttpStatus.NOT_FOUND)
        }

        await this.userRepository.update(
            {role: 'admin'},
            {where: {email: request.email}})

        const user: UserModel[] = await this.findByEmail(request);

        return user;
    }


    async findByLogin(loginDto: LoginDto) {
        const user = await this.userRepository.findOne({
            where: {
                email: loginDto.email
            }
        });
        if (!user) {
            throw new HttpException(apiMessagesEnum.invalidCredentials, HttpStatus.UNAUTHORIZED);
        }

        if (await bcrypt.compare(loginDto.password, user.password)) {
            return this.sanitizeUser(user)
        } else {
            throw new HttpException(apiMessagesEnum.invalidCredentials, HttpStatus.UNAUTHORIZED);
        }
    }

    async checkIfAdmin(token) {
        await jwt.verify(token, process.env.SECRET_KEY,
            function (err, decoded) {
                if (decoded.role !== 'admin') {
                    throw new HttpException(apiMessagesEnum.unauthorized, HttpStatus.FORBIDDEN)
                }
            })
    }

    async userExists(userId): Promise<boolean> {
        const user = await this.userRepository.findByPk(userId)

        if(!user){
            return false
        } else {
            return true
        }
    }

    sanitizeUser(user: UserRegisterDto) {
        const sanitized = user;
        delete sanitized['password'];
        return sanitized
    }

    async getUserTasks(dto: UserIdDto) {
        const user = await this.userRepository.findOne({
            where: {
                id : dto.id
            },
            include: [{
                model: TasksModel
            }]
        })
        return user
    }
}
