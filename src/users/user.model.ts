import {Model, Table, Column, DataType, BeforeCreate, PrimaryKey, HasMany} from 'sequelize-typescript'
import {TasksModel} from "../tasks/tasks.model";
const uuid = require('uuid').v4
const bcrypt = require('bcrypt')


@Table({tableName: 'user'})
export class UserModel extends Model<UserModel> {

  @PrimaryKey
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
    primaryKey: true,
    unique: true
  })
  id: string;

  @HasMany(() => TasksModel)
  tasks: TasksModel[]

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: false
  })
  firstName: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: false
  })
  lastName: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: true
  })
  email: string

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: false
  })
  password: string

  @Column({
      type: DataType.BOOLEAN,
      allowNull: false,
      unique: false,
      defaultValue: true
  })
  isActive: boolean;

  @Column({
      type: DataType.STRING,
      allowNull: false,
      unique: false,
      defaultValue: 'user' // user / mod / admin
  })
  role: string;

  @BeforeCreate
  public static async prepareCreate(user: UserModel) {
    user.id = uuid()
    user.password = await bcrypt.hash(user.password, 10)
  }
}
