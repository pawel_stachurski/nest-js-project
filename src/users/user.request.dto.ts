import { ApiProperty } from '@nestjs/swagger';
import {IsEmail, IsNotEmpty, IsOptional, IsString, IsUUID} from 'class-validator';

export class UserIdDto {

  @ApiProperty({example: '417302f6-4ff3-4c4e-86cd-7522cb87dc8a'})
  @IsNotEmpty()
  @IsUUID('4')
  id: string;

}

export class UserEmailRequestDto {

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

}

export class LoginDto extends UserEmailRequestDto {

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  password: string

}

export class UserRegisterDto extends LoginDto {

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  readonly firstName: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  readonly lastName: string;

  @IsString()
  @IsOptional()
  role: string;

  @IsOptional()
  isActive: boolean;

}

export interface Payload {
  email: string;
  firstName: string;
  lastName: string;
  role: string;
  isActive: boolean;
  iat?: Date;
  expiresIn?: string;
}
