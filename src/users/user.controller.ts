import {Body, Controller, Get, Patch, Post, Query, Req} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {UserService} from "./user.service";
import {Request} from 'express';
import {UserEmailRequestDto, UserIdDto} from "./user.request.dto";
import {apiPathsEnum, apiTagsEnum} from "../../libs/enum/src";

@ApiTags(apiTagsEnum.users)
@Controller(apiPathsEnum.users)
export class UserController {

    constructor(
        private userService: UserService
    ) {
    }

    @Post()
    async getUserByEmail(@Body() userEmailRequestDto: UserEmailRequestDto) {
        return this.userService.findByEmail(userEmailRequestDto);
    }

    @Get(apiPathsEnum.getAll)
    async getAllUsers() {
        return this.userService.getAllUsers();
    }

    @Get(apiPathsEnum.getUserTasks)
    async getUserTasks(@Query() dto: UserIdDto) {
        return this.userService.getUserTasks(dto);
    }

    // @Patch('deactivate')
    // async deactivateUser(
    //     @Body() userEmailRequestDto: UserEmailRequestDto,
    //     @Req() request: Request
    // ) {
    //     return await this.userService.deactivateUser(
    //         userEmailRequestDto,
    //         request.header('token'));
    // }
    //
    // @Patch('promote')
    // async promoteUser(
    //     @Body() userEmailRequestDto: UserEmailRequestDto,
    //     @Req() request: Request
    // ) {
    //     return await this.userService.promoteUser(userEmailRequestDto, request.header('token'));
    // }

}
