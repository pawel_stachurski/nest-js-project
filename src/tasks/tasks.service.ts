import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {AssignUserDto, TaskCreateDto} from "./tasks.dto";
import {InjectModel} from "@nestjs/sequelize";
import {TasksModel} from "./tasks.model";
import {apiMessagesEnum} from "../../libs/enum/src";
import {UserService} from "../users/user.service";

@Injectable()
export class TasksService {

    constructor(
        @InjectModel(TasksModel) private tasksRepository: typeof TasksModel,
        private userService: UserService,
    ) {
    }

    getAllProjects() {
        return this.tasksRepository.findAll()
    }

    async createNewProject(dto: TaskCreateDto): Promise<TasksModel> {
        return this.tasksRepository.create(dto);
    }

    async assignUserToTask(dto: AssignUserDto): Promise<TasksModel> {
        const task = await this.tasksRepository.findOne({
            where: {
                id: dto.taskId
            }
        })
        if (!task) {
            throw new HttpException(apiMessagesEnum.taskNotFound, HttpStatus.NOT_FOUND)
        }
        if (!await this.userService.userExists(dto.assigneeId)){
            throw new HttpException(apiMessagesEnum.userNotFound, HttpStatus.NOT_FOUND)
        }

        task.update({
            assigneeId : dto.assigneeId
        })

        await task.save;

        return task
    }
}
