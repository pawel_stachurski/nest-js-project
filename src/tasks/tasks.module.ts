import {Module} from '@nestjs/common';
import {SequelizeModule} from '@nestjs/sequelize';
import {TasksModel} from "./tasks.model";
import {TasksController} from "./tasks.controller";
import {TasksService} from "./tasks.service";
import {UserModule} from "../users/user.module";

@Module({
    imports: [
        SequelizeModule.forFeature([TasksModel]),
        UserModule],
    controllers: [TasksController],
    providers: [TasksService],
    exports: []
})
export class TasksModule {
}
