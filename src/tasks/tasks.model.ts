import {
    Model,
    Table,
    Column,
    DataType,
    BeforeCreate,
    ForeignKey,
    BelongsTo,
    PrimaryKey,
    HasMany, HasOne, BelongsToMany
} from 'sequelize-typescript'
import {v4 as uuid} from 'uuid';
import {UserModel} from "../users/user.model";
import {ProjectsModel} from "../projects/projects.model";
import {ColumnsModel} from "../columns/columns.model";

@Table({tableName: 'tasks'})
export class TasksModel extends Model<TasksModel> {

    @PrimaryKey
    @Column({
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
        allowNull: false,
        primaryKey: true,
        unique: true
    })
    id: string;

    // @BelongsToMany(() => ProjectsModel, {through: 'project-taskIds'})
    // projects: ProjectsModel[];

    // @HasMany(() => ProjectsModel)
    // projects: ProjectsModel[];

    // @HasOne(() => ColumnsModel)
    // column: ColumnsModel;

    @Column({
        type: DataType.STRING,
        allowNull: false,
        unique: false,
    })
    title: string;

    @Column({
        type: DataType.TEXT,
        allowNull: true,
        unique: false,
    })
    description: string;

    @Column({
        type: DataType.INTEGER,
        allowNull: false,
        autoIncrement: true,
        unique: true,
    })
    order: number;

    @Column({
        type: DataType.DATE,
        allowNull: true,
        unique: false,
    })
    dueDate: Date;

    @Column({
        type: DataType.BOOLEAN,
        defaultValue: false,
        allowNull: false,
        unique: false,
    })
    isComplete: boolean;

    @ForeignKey(() => UserModel)
    @Column({
        type: DataType.UUID,
        allowNull: true,
        unique: false,
    })
    assigneeId: string;

    @BelongsTo(() => UserModel)
    assignee: UserModel

    @BeforeCreate
    public static async prepareCreate(task: TasksModel) {
        if (task.id === null || task.id === undefined) {
            task.id = uuid()
        }
    }

}
