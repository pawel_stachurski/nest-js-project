import {ApiProperty} from "@nestjs/swagger";
import {IsDateString, IsNotEmpty, IsOptional, IsString, IsUUID} from "class-validator";

export class TaskCreateDto{

    @ApiProperty({example: 'Testowa tytuł'})
    @IsString()
    @IsNotEmpty()
    title: string;

    // @ApiProperty({example: 'Opcjonalny opis'})
    @IsString()
    @IsOptional()
    description?: string;

    // @ApiProperty({example: '2021-05-16T22:41:19'})
    @IsDateString()
    @IsOptional()
    dueDate?: Date

    // @ApiProperty({example: 'bf96337f-5a72-4193-85b4-f76dfb23e044'})
    @IsUUID()
    @IsOptional()
    assigneeId?: string;
}

export class AssignUserDto{

    @ApiProperty({example: '7bd28025-9ba7-4b52-b50e-1020c5070623'})
    @IsUUID()
    @IsNotEmpty()
    taskId: string;

    @ApiProperty({example: '417302f6-4ff3-4c4e-86cd-7522cb87dc8a'})
    @IsUUID()
    @IsNotEmpty()
    assigneeId: string;
}
