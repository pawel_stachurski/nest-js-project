import {Body, Controller, Get, Post} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {TasksService} from "./tasks.service";
import {AssignUserDto, TaskCreateDto} from "./tasks.dto";
import {apiPathsEnum, apiTagsEnum} from "../../libs/enum/src";


@ApiTags(apiTagsEnum.tasks)
@Controller(apiPathsEnum.tasks)
export class TasksController {

    constructor(
        private tasksService: TasksService
    ) {
    }

    @Get(apiPathsEnum.getAll)
    async getAllTasks(){
        return this.tasksService.getAllProjects();
    }

    @Post(apiPathsEnum.create)
    async createNewTask(@Body() dto: TaskCreateDto){
        return await this.tasksService.createNewProject(dto);
    }

    @Post(apiPathsEnum.assignUser)
    async assignUserToTask(@Body() dto: AssignUserDto){
        return await this.tasksService.assignUserToTask(dto);
    }

}
