import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsString} from "class-validator";

export class ProjectCreateDto {

    @ApiProperty({example: 'Testowa nazwa'})
    @IsString()
    @IsNotEmpty()
    name: string;
}

export class ProjectIdDto {

    @ApiProperty({example: '813903c4-76fc-4cd5-a334-66983ee2099d'})
    @IsString()
    @IsNotEmpty()
    projectId: string;
}

export class AssignTasksDto extends ProjectIdDto{

    @ApiProperty({example: 'f0e31757-7e27-4f23-83de-7986cf42287c'})
    @IsString()
    @IsNotEmpty()
    taskId: string;
}
