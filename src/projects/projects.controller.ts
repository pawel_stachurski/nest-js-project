import {Body, Controller, Get, Post, Query} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {ProjectsService} from "./projects.service";
import {AssignTasksDto, ProjectCreateDto} from "./projects.dto";
import {apiPathsEnum, apiTagsEnum} from "../../libs/enum/src";


@ApiTags(apiTagsEnum.projects)
@Controller(apiPathsEnum.projects)
export class ProjectsController {

    constructor(
        private projectsService: ProjectsService
    ) {
    }

    @Get(apiPathsEnum.getAll)
    async getAllProjects(){
        return this.projectsService.getAllProjects();
    }

    @Post(apiPathsEnum.create)
    async createNewProject(@Body() dto: ProjectCreateDto){
        return await this.projectsService.createNewProject(dto);
    }

    @Get('assignTask')
    async assignTask(@Query() dto: AssignTasksDto){
        return this.projectsService.assignTaskToProject(dto)
    }

}
