import {
  BeforeCreate,
  BelongsTo, BelongsToMany,
  Column,
  DataType,
  ForeignKey,
  HasMany,
  Model,
  PrimaryKey,
  Table
} from 'sequelize-typescript'
import { v4 as uuid } from 'uuid';
import {TasksModel} from "../tasks/tasks.model";
import {ColumnsModel} from "../columns/columns.model";

@Table({tableName: 'projects'})
export class ProjectsModel extends Model<ProjectsModel> {

  @PrimaryKey
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
    primaryKey: true,
    unique: true
  })
  id: string;

  @HasMany(() => ColumnsModel)
  columns: ColumnsModel[];


  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: false,
  })
  name: string;

  // @ForeignKey(() => TasksModel)
  @Column({
    type: DataType.ARRAY(DataType.UUID),
    allowNull: true,
    unique: false,
  })
  tasksIds: string[];

  // @BelongsToMany(() => TasksModel, {through: 'project-taskIds'})
  // tasks: TasksModel[]
  // @BelongsTo(() => TasksModel)


  @BeforeCreate
  public static async prepareCreate(project: ProjectsModel) {
     if (project.id === null || project.id === undefined) {
      project.id = uuid()
    }
  }

}
