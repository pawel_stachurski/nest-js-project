import {Module} from '@nestjs/common';
import {SequelizeModule} from '@nestjs/sequelize';
import {ProjectsModel} from "./projects.model";
import {ProjectsController} from "./projects.controller";
import {ProjectsService} from "./projects.service";

@Module({
  imports: [SequelizeModule.forFeature([ProjectsModel])],
  controllers: [ProjectsController],
  providers: [ProjectsService],
  exports: []
})
export class ProjectsModule {}
