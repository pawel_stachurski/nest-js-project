import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {ProjectsModel} from "./projects.model";
import {AssignTasksDto, ProjectCreateDto} from "./projects.dto";
import {apiMessagesEnum} from "../../libs/enum/src";
import * as lodash from "lodash";

@Injectable()
export class ProjectsService {

    constructor(
        @InjectModel(ProjectsModel) private projectsRepository: typeof ProjectsModel
    ) {
    }

    async getAllProjects() {
        return this.projectsRepository.findAll();
    }

    async createNewProject(dto: ProjectCreateDto): Promise<ProjectsModel> {
        return this.projectsRepository.create(dto);
    }

    async assignTaskToProject(dto: AssignTasksDto) {
        const project = await this.projectsRepository.findOne({
            where: {
                id: dto.projectId
            }
        })
        if(!project){
            throw new HttpException(apiMessagesEnum.projectNotFound, HttpStatus.NOT_FOUND)

        }

        if(project.tasksIds === null) {
            const newArray : string[] = [];
            newArray.push(dto.taskId);
            project.tasksIds = newArray;
        } else {
            //todo check if taskID is unique
           project.tasksIds.push(dto.taskId)
           project.update({
               tasksIds: project.tasksIds
           })
        }
        project.save()

        return project

    }
}
