import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/sequelize";
import {ColumnsModel} from "./columns.model";
import {ColumnCreateDto} from "./columns.dto";
import {apiMessagesEnum} from "../../libs/enum/src";

@Injectable()
export class ColumnsService {

    constructor(
        @InjectModel(ColumnsModel) private columnsRepository: typeof ColumnsModel
    ) {
    }

    getAllProjects() {
        return this.columnsRepository.findAll()
    }

    async createNewProject(dto: ColumnCreateDto): Promise<ColumnsModel | string> {
        const checkColumnsNames = await this.columnsRepository.findOne({
            where: {
                projectId: dto.projectId,
                name: dto.name
            }
        })
        if (checkColumnsNames){
            return apiMessagesEnum.columnExists
        }

        return this.columnsRepository.create(dto);
    }
}
