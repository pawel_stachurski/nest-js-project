import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsString, IsUUID} from "class-validator";

export class ColumnCreateDto {

    @ApiProperty({example: 'Testowa nazwa'})
    @IsString()
    @IsNotEmpty()
    name: string;

    @ApiProperty({example: '813903c4-76fc-4cd5-a334-66983ee2099d'})
    @IsUUID()
    @IsNotEmpty()
    projectId: string;
}
