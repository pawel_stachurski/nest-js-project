import {Body, Controller, Get, Post} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";
import {ColumnsService} from "./columns.service";
import {ColumnCreateDto} from "./columns.dto";
import {apiPathsEnum, apiTagsEnum} from "../../libs/enum/src";


@ApiTags(apiTagsEnum.columns)
@Controller(apiPathsEnum.columns)
export class ColumnsController {

    constructor(
        private columnsService: ColumnsService
    ) {
    }

    @Get(apiPathsEnum.getAll)
    async getAllColumns(){
        return this.columnsService.getAllProjects();
    }

    @Post(apiPathsEnum.create)
    async createNewColumn(@Body() dto: ColumnCreateDto){
        return await this.columnsService.createNewProject(dto);
    }

}
