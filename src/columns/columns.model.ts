import {BeforeCreate, BelongsTo, Column, DataType, ForeignKey, Model, Table} from 'sequelize-typescript'
import { v4 as uuid } from 'uuid';
import {ProjectsModel} from "../projects/projects.model";
import {TasksModel} from "../tasks/tasks.model";

@Table({tableName: 'columns'})
export class ColumnsModel extends Model<ColumnsModel> {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
    primaryKey: true,
    unique: true
  })
  id: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: false,
  })
  name: string;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
    autoIncrement:true,
    unique: true,
  })
  order: number;

  @ForeignKey(() => ProjectsModel)
  @Column({
    type: DataType.UUID,
    allowNull: false,
    unique: false,
  })
  projectId: string;

  @BelongsTo(() => ProjectsModel)
  project: ProjectsModel;

  // @ForeignKey(() => TasksModel)
  @Column({
    type: DataType.ARRAY(DataType.UUID),
    allowNull: true,
    unique: false,
  })
  tasksIds: string;

  // @BelongsTo(() => TasksModel)
  // tasks: TasksModel[]

  @BeforeCreate
  public static async prepareCreate(column: ColumnsModel) {
    if (column.id === null || column.id === undefined) {
      column.id = uuid()
    }
  }


}
