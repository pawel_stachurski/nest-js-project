import {Module} from '@nestjs/common';
import {SequelizeModule} from '@nestjs/sequelize';
import {ColumnsModel} from "./columns.model";
import {ColumnsService} from "./columns.service";
import {ColumnsController} from "./columns.controller";

@Module({
  imports: [SequelizeModule.forFeature([ColumnsModel])],
  controllers: [ColumnsController],
  providers: [ColumnsService],
  exports: []
})
export class ColumnsModule {}
