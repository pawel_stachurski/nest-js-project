import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix('api');

  const swaggerOptions = new DocumentBuilder()
      .setTitle('Swagger - Kanban board')
      .setDescription(' ')
      .setVersion('1.0')
      .addBearerAuth({
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'jwt'
      })
      .build();
  const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
  SwaggerModule.setup('api', app, swaggerDocument, {
    swaggerOptions: {
      docExpansion: 'none'
    }
  });

  await app.listen(process.env.PORT);
}
bootstrap();
