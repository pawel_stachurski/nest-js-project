import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';

import {SequelizeModule} from "@nestjs/sequelize";
import {Sequelize} from "sequelize-typescript";
import {ConfigModule} from "@nestjs/config";
import {UserModule} from "./users/user.module";
import {AuthModule} from "./auth/auth.module";
import {ProjectsModule} from "./projects/projects.module";
import {ColumnsModule} from "./columns/columns.module";
import {TasksModule} from "./tasks/tasks.module";

@Module({
    imports: [
        ConfigModule.forRoot({
            isGlobal: true
        }),
        SequelizeModule.forRoot({
            dialect: 'postgres',
            host: process.env.DATABASE_HOST,
            port: Number(process.env.DATABASE_PORT),
            username: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_DB,
            autoLoadModels: true,
            synchronize: true,
        }),
        UserModule,
        AuthModule,
        ProjectsModule,
        ColumnsModule,
        TasksModule
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
  constructor(private sequelize: Sequelize) {
  }
}
