import { Injectable } from '@nestjs/common';
import { UserService } from '../users/user.service';
import { Payload } from '../users/user.request.dto';
import { ConfigService } from '@nestjs/config';
import { sign } from 'jsonwebtoken';


@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private config: ConfigService
  ) {}

  async signPayload(payload: Payload) {
    return sign(
      payload,
      this.config.get('SECRET_KEY'), {
      expiresIn: this.config.get('EXPIRESIN')
    })
  }

  async validateUser(payload: Payload) {
    return this.userService.findByEmail({email: payload.email})
  }


}
