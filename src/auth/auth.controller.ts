import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserService } from '../users/user.service';
import { ApiTags } from '@nestjs/swagger';
import { LoginDto, Payload, UserRegisterDto } from '../users/user.request.dto';
import {apiPathsEnum, apiTagsEnum} from "../../libs/enum/src";

@ApiTags(apiTagsEnum.users)
@Controller(apiPathsEnum.auth)
export class AuthController {
  constructor(
    private userService: UserService,
    private authService: AuthService
  ) {}

  @Post(apiPathsEnum.login)
  async login(@Body() loginDto: LoginDto) {
    const user = await this.userService.findByLogin(loginDto);
    const payload: Payload = {
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      role: user.role,
      isActive: user.isActive,
    };
    const token = await this.authService.signPayload(payload);
    return { token };
  }

  @Post(apiPathsEnum.register)
  async register(@Body() userDto: UserRegisterDto) {
    const user = await this.userService.create(userDto);
    const payload: Payload = {
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      role: user.role,
      isActive: user.isActive,
    };
    const token = await this.authService.signPayload(payload);
    return { token }
  }


}
