// import {IsNotEmpty, IsNumber, IsOptional, IsPositive, IsString, MinLength} from "class-validator";
//
//
// export class AddProductDTO {
//
//     @IsNotEmpty()
//     @IsString()
//     @MinLength(2)
//     name: string;
//
//     @IsString()
//     description: string;
//
//     @IsPositive()
//     @IsNotEmpty()
//     @IsNumber()
//     price: number;
//
//     @IsPositive()
//     @IsOptional()
//     @IsNumber()
//     quantity: number;
// }
//
// export class AddQuantityDTO {
//
//     @IsPositive()
//     @IsNotEmpty()
//     @IsNumber()
//     quantity: number;
// }
//
// export class UpdateInfoDTO {
//
//     @IsPositive()
//     @IsOptional()
//     @IsNumber()
//     price: number;
//
//     @IsOptional()
//     @IsString()
//     description: string;
// }