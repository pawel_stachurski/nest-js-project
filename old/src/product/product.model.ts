// import {Model, Table, Column, DataType, BeforeCreate} from 'sequelize-typescript'
// const uuid = require('uuid').v4
//
//
// @Table({tableName: 'product'})
// export class ProductModel extends Model<ProductModel> {
//     @Column({
//         type: DataType.INTEGER,
//         defaultValue: DataType.INTEGER,
//         autoIncrement: true,
//         allowNull: false,
//         primaryKey: true,
//         unique: true
//     })
//     id: number;
//
//     // @Column({
//     //     type: DataType.UUID,
//     //     defaultValue: DataType.UUIDV4,
//     //     allowNull: false,
//     //     primaryKey: true,
//     //     unique: true
//     // })
//     // id: string;
//
//     @Column({
//         type: DataType.STRING,
//         allowNull: false,
//         unique: true
//     })
//     name: string;
//
//     @Column({
//         type: DataType.STRING,
//         defaultValue: '',
//         allowNull: true,
//         unique: false
//     })
//     description: string;
//
//     @Column({
//         type: DataType.INTEGER,
//         allowNull: false,
//         unique: false
//     })
//     price: number;
//
//     @Column({
//         type: DataType.INTEGER,
//         defaultValue: 0,
//         allowNull: true,
//         unique: false
//     })
//     quantity: number;
//
//     // @Column({nullable: true, default: null})
//     // image: null | string;
//     // @Column({
//     //     type: DataType.STRING,
//     //     allowNull: false,
//     //     unique: false
//     // })
//     // image: string;
//
//     // @BeforeCreate
//     // public static async prepareCreate(product: ProductModel) {
//     //     product.id = uuid()
//     // }
// }
