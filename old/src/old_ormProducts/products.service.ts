// import {
//     ConflictException,
//     Injectable,
//     InternalServerErrorException,
//     NotFoundException,
//     UnauthorizedException
// } from "@nestjs/common";
// import {InjectRepository} from "@nestjs/typeorm";
// import {Repository} from "typeorm";
// import {ProductEntity} from "../old_entities/product.entity";
// import {AddProductDTO, AddQuantityDTO, UpdateInfoDTO} from "../old_models/product.dto";
// import {UserEntity} from "../old_entities/user.entity";
// import {InjectModel} from "@nestjs/sequelize";
// import {ProductModel} from "../old_entities/product.model";
//
// @Injectable()
// export class ProductsService {
//
//     constructor(
//         @InjectModel(ProductModel) private productsList: typeof ProductModel
//         // @InjectRepository(ProductEntity)
//         // private productsList: Repository<ProductEntity>,
//         // @InjectRepository(UserEntity)
//         // private usersList: Repository<UserEntity>
//     ) {
//     }
//
//     // getAllProducts(): Promise<ProductEntity[]> {
//     //     return this.productsList.find();
//     // }
//     //
//     // async getProduct(name: string): Promise<ProductEntity> {
//     //     try {
//     //         const product = await this.productsList.findOneOrFail({where: {name}});
//     //         return {...product};
//     //     } catch (err) {
//     //         throw new NotFoundException('Product not found!');
//     //     }
//     // }
//     //
//     // async addProduct(requestJSON: AddProductDTO, token: string) {
//     //
//     //     await this.checkIfModOrAdmin(token);
//     //
//     //     try {
//     //         const product = await this.productsList.create(requestJSON);
//     //         await this.productsList.save(requestJSON);
//     //         return {message: 'Product added!', product};
//     //
//     //     } catch (err) {
//     //         throw new ConflictException("Product with given name already exist");
//     //     }
//     // }
//     //
//     // async addQuantity(name: string, quantity: AddQuantityDTO, token: string) {
//     //
//     //     await this.checkIfModOrAdmin(token);
//     //     const product = await this.productsList.findOne({where: {name}});
//     //     if (!product) {
//     //         throw new NotFoundException('Product not found!');
//     //     }
//     //
//     //     try {
//     //         await this.productsList.update(
//     //             {name},
//     //             {quantity: product.quantity + quantity.quantity})
//     //         product.quantity = product.quantity + quantity.quantity;
//     //
//     //
//     //         return {message: 'Product quantity updated', product};
//     //     } catch (err) {
//     //         throw new InternalServerErrorException('Oops! Something went wrong')
//     //     }
//     // }
//     //
//     // async updateInfo(name: string, {price, description}: UpdateInfoDTO, token: string) {
//     //
//     //     await this.checkIfModOrAdmin(token);
//     //     const product = await this.productsList.findOne({where: {name}});
//     //     if (!product) {
//     //         throw new NotFoundException('Product not found!');
//     //     }
//     //
//     //     try {
//     //         await this.productsList.update(
//     //             {price, description},
//     //             {price, description});
//     //
//     //         product.price = price;
//     //         product.description = description;
//     //         return {message: 'Product information updated', product};
//     //
//     //     } catch (err) {
//     //         throw new InternalServerErrorException('Oops! Something went wrong')
//     //     }
//     // }
//     //
//     // async deleteProduct(name: string, token: string) {
//     //
//     //     await this.checkIfModOrAdmin(token); // zmienić na tylko dla admina
//     //     const product = await this.productsList.findOne({where: {name}});
//     //     if (!product) {
//     //         throw new NotFoundException('Product not found!');
//     //     }
//     //
//     //     try {
//     //         await this.productsList.delete({name})
//     //
//     //         return {message: 'Product deleted', product};
//     //
//     //     } catch (err) {
//     //         throw new InternalServerErrorException('Oops! Something went wrong')
//     //     }
//     //
//     //
//     // }
//     //
//     // private async checkIfModOrAdmin(token: string) {
//     //     const userWithGivenToken = await this.usersList.findOne({where: {token}})
//     //     if (!userWithGivenToken) {
//     //         throw new NotFoundException('Wrong token')
//     //     }
//     //     if (userWithGivenToken.role !== 'admin' && userWithGivenToken.role !== 'mod') {
//     //         throw new UnauthorizedException("You need permission to do that")
//     //     }
//     // }
//     //
//     // private async checkIfAdmin(token: string) {
//     //     const userWithGivenToken = await this.usersList.findOne({where: {token}})
//     //     if (!userWithGivenToken) {
//     //         throw new NotFoundException('Wrong token')
//     //     }
//     //     if (userWithGivenToken.role !== 'admin' && userWithGivenToken.role !== 'mod') {
//     //         throw new UnauthorizedException("You need permission to do that")
//     //     }
//     // }
// }
