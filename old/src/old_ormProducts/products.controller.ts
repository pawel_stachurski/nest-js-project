// import {Controller, Post, Body, Get, Param, Patch, Delete, Req, Query, ValidationPipe} from "@nestjs/common";
// import {ProductsService} from "./products.service";
// import {ApiOperation, ApiTags} from "@nestjs/swagger";
// import {Request} from 'express';
// import {AddProductDTO, AddQuantityDTO, UpdateInfoDTO} from "../old_models/product.dto";
// import {ProductEntity} from "../old_entities/product.entity";
//
// @ApiTags('Products controller')
// @Controller('products')
// export class ProductsController {
//
//     constructor(private productsService: ProductsService) {
//     }
//
//     // @Get()
//     // getAllProducts(): Promise<ProductEntity[]> {
//     //     return this.productsService.getAllProducts();
//     // }
//     //
//     // @Get(':name')
//     // async getProduct(@Param('name') name: string): Promise<ProductEntity> {
//     //     return this.productsService.getProduct(name);
//     // }
//     //
//     // @Post('add')
//     // async addProduct( //admin+mod
//     //     @Body(ValidationPipe) addProductDTO: AddProductDTO,
//     //     @Req() request: Request,
//     // ) {
//     //     return await this.productsService.addProduct(addProductDTO, request.header('token'));
//     // }
//     //
//     // @Patch('add-quantity/:name')
//     // async addQuantity( //admin+mod
//     //     @Param('name') name: string,
//     //     @Body(ValidationPipe) quantityDTO: AddQuantityDTO,
//     //     @Req() request: Request,
//     // ) {
//     //     return await this.productsService.addQuantity(name, quantityDTO, request.header('token'));
//     // }
//     //
//     // @Patch('update-info/:name')
//     // async updateInfo(
//     //     @Param('name') name: string,
//     //     @Body(ValidationPipe) updateInfoDTO: UpdateInfoDTO,
//     //     @Req() request: Request,
//     // ) {
//     //     return await this.productsService.updateInfo(name, updateInfoDTO, request.header('token'));
//     // }
//     //
//     // @Delete('delete/:name')
//     // async deleteProduct(
//     //     @Param('name') name: string,
//     //     @Req() request: Request,
//     // ) {
//     //     return await this.productsService.deleteProduct(name, request.header('token'));
//     // }
// }
