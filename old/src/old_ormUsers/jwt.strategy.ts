// import {HttpException, HttpStatus, Injectable, UnauthorizedException} from "@nestjs/common";
// import {PassportStrategy} from "@nestjs/passport";
// import {ExtractJwt, Strategy, VerifiedCallback} from "passport-jwt";
// import {InjectRepository} from "@nestjs/typeorm";
// import {UserEntity} from "../old_entities/user.entity";
// import {Repository} from "typeorm";
// import {AuthPayLoad} from "../old_models/user.dto";
// import {ConfigService} from "@nestjs/config";
// import {UsersService} from "./users.service";
//
// @Injectable()
// export class JwtStrategy extends PassportStrategy(Strategy){
//     constructor(
//         // @InjectRepository(UserEntity)
//         // private usersList: Repository<UserEntity>,
//         private config: ConfigService,
//         private userService: UsersService
//     ) {
//         super({
//             jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('token'),
//             // jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), //todo
//             secretOrKey: config.get('SECRET_KEY')
//         });
//     }
//
//     // async validate(payload: AuthPayLoad) {
//     //     const {email} = payload;
//     //     const user = this.usersList.find({where: {email}})
//     //     if(!user) {
//     //         throw new UnauthorizedException('Unauthorized')
//     //     }
//     //     return user;
//     // }
//
//     async validate(payload: AuthPayLoad, done: VerifiedCallback) {
//         const user = await this.userService.validateUser(payload);
//
//         if (!user) {
//             return done(
//                 new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED),
//                 false,
//             );
//         }
//
//         return done(null, user)
//     }
//
//
// }
