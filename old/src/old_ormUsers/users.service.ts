// import {
//     ConflictException, HttpException, HttpStatus,
//     Injectable,
//     InternalServerErrorException,
//     NotFoundException,
//     UnauthorizedException
// } from "@nestjs/common";
// // import {UserEntity} from "../entities/user.entity";
// import {InjectRepository} from "@nestjs/typeorm";
// import {UserEntity} from "../old_entities/user.entity";
// import {Repository} from "typeorm";
// import {AuthPayLoad, LoginDTO, RegisterDTO, UserEmailRequestDto} from "../old_models/user.dto";
// import {JwtService} from "@nestjs/jwt";
// import * as bcrypt from "bcryptjs";
// import {request} from "express";
// import {sign} from "jsonwebtoken";
// import {ConfigService} from "@nestjs/config";
// import {InjectModel} from "@nestjs/sequelize";
// import {UserModel} from "../old_entities/user.model";
//
// @Injectable()
// export class UsersService {
//
//     constructor(
//         // @InjectRepository(UserEntity)
//         // private usersList: Repository<UserEntity>,
//         @InjectModel(UserModel) private usersList: typeof UserModel,
//         // private jwtService: JwtService,
//         private config: ConfigService,
//     ) {
//     }
//
//     // findAll(): Promise<UserEntity[]> {
//     //     return this.usersList.find();
//     // }
//
//     // async register(credentials: RegisterDTO) {
//     //     try {
//     //         const user = await this.usersList.findOne({
//     //             where: {email: credentials.email}});
//     //         if (user) {
//     //             throw new HttpException('Email has already been taken', HttpStatus.BAD_REQUEST);
//     //         }
//     //
//     //         const createdUser = new this.usersList(credentials);
//     //         await createdUser.save()
//     //         return createdUser;
//     //
//     //
//     //
//     //         // const payload = {email: user.email};
//     //         // const token = this.jwtService.sign(payload)
//     //         // return {user: {...user, token}};
//     //     } catch (err) {
//     //         throw new ConflictException('Email has already been taken');
//     //     }
//     // }
//
//
//     // async login({email, password}: LoginDTO) {
//     //     const user = await this.usersList.findOne({where: {email}});
//     //     if (!user) {
//     //         throw new NotFoundException("User with given email doesn't exist");
//     //     }
//     //     const isValid = await user.comparePassword(password);
//     //     if (!isValid) {
//     //         throw new UnauthorizedException("Wrong password");
//     //     }
//     //     if (user.isActive === false) {
//     //         throw new UnauthorizedException('User with given credentials is NOT active')
//     //     }
//     //     const payload = {email: user.email};
//     //     const token = this.jwtService.sign(payload);
//     //     await this.usersList.update(user, {token: token});
//     //     return {user: {...user, token}};
//     // }
//
//     // findOne(id: number): Promise<UserEntity> {
//     //     try {
//     //         return this.usersList.findOneOrFail(id);
//     //
//     //     } catch (err) {
//     //         throw new NotFoundException('User not found!');
//     //     }
//     // }
//
//     async findByEmail(request: UserEmailRequestDto) {
//         return this.usersList.findOne({
//             where: {
//                 email: request.email
//             }
//         })
//     }
//
//     // findOne(id: string): Promise<OrmUser> {
//     //     return this.usersList.findOne(id);
//     // }
//
//     // async deactivateUser(id: number, token): Promise<UserEntity> {
//     //
//     //     const userWithGivenToken = await this.usersList.findOne({where: {token}})
//     //     if(!userWithGivenToken) {
//     //         throw new NotFoundException('Wrong token')
//     //     }
//     //
//     //     if(userWithGivenToken.role !== 'admin'){
//     //         throw new UnauthorizedException("You need permission to do that")
//     //     }
//     //
//     //     try {
//     //         await this.usersList.update(id, {isActive: false});
//     //         const user = await this.usersList.findOneOrFail(id);
//     //
//     //         return user
//     //
//     //     } catch (err) {
//     //         throw new NotFoundException('User not found!')
//     //     }
//     // }
//
//     // async removeUser(id: number, token) {
//     //
//     //     const userWithGivenToken = await this.usersList.findOne({where: {token}})
//     //     if(!userWithGivenToken) {
//     //         throw new NotFoundException('Wrong token')
//     //     }
//     //
//     //     if(userWithGivenToken.role !== 'admin'){
//     //         throw new UnauthorizedException("You need permission to do that")
//     //     }
//     //
//     //     try {
//     //         const user = await this.usersList.findOneOrFail(id);
//     //         await this.usersList.delete(id);
//     //         return user;
//     //
//     //     } catch (err) {
//     //         throw new NotFoundException('User not found')
//     //     }
//     // }
//
//     async signPayload(payload: AuthPayLoad) {
//         return sign(
//             payload,
//             this.config.get('SECRET_KEY'), {
//                 expiresIn: this.config.get('EXPIRESIN')
//             })
//     }
//
//     async validateUser(payload: AuthPayLoad) {
//         return this.findByEmail({email: payload.email})
//     }
//
//
//
// }
