// import {Controller, Post, Body, Get, Param, Patch, Delete, Query} from "@nestjs/common";
// import {ProductsService} from "./products.service";
// import {ApiOperation} from "@nestjs/swagger";
// import {AddProductDto} from "./addProductDto";
//
// @Controller('products')
// export class ProductsController {
//
//     constructor(private productsService: ProductsService) {
//     }
//
//     @Post()
//     @ApiOperation({
//         summary: 'Add a new product',
//         description: 'This endpoint will be used to add new product to products list'
//     })
//     addProduct(@Body() addProductDto: AddProductDto) {
//         this.productsService.insertProduct(addProductDto)
//     }
//     // addProduct(
//     //     @Body('title') prodTitle: string,
//     //     @Body('description') prodDesc: string,
//     //     @Body('price') prodPrice: number,
//     //     ) {
//     //     const generatedId = this.productsService.insertProduct(prodTitle, prodDesc, prodPrice);
//     //     return {id: generatedId};
//     // }
//
//
//     @Get()
//     @ApiOperation({
//         summary: 'Get list of all products',
//         description: 'This endpoint will provide list of all products'
//     })
//     getAllProducts() {
//         // return {product: this.productsService.getProducts()};
//         return this.productsService.getProducts();
//     }
//
//     // @Get('id')
//     // @ApiOperation({
//     //     summary: 'Get product by ID',
//     //     description: 'This endpoint will provide single product by ID'
//     // })
//     // getProduct(@Query() getProductDto: ProductIdDto) {
//     //     const id = this.productsService.getSingleProduct(getProductDto);
//     //     // return this.productsService.getSingleProduct(getProductDto);
//     // }
//     @Get(':id')
//     getProduct(@Param('id') prodId: string) {
//         return this.productsService.getSingleProduct(prodId)[0];
//     }
//
//     @Patch(':id')
//     updateProduct(
//         @Param('id') prodId: string,
//         @Body('title') prodTitle: string,
//         @Body('description') prodDesc: string,
//         @Body('price') prodPrice: number) {
//
//         this.productsService.updateProduct(prodId, prodTitle, prodDesc, prodPrice);
//         return "Product updated";
//     }
//
//     @Delete(':id')
//     removeProduct(@Param('id') prodId: string) {
//
//         this.productsService.deleteProduct(prodId);
//         return "Product deleted";
//     }
//
// }
