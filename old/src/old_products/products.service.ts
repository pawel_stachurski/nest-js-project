// import {Injectable, NotFoundException} from "@nestjs/common";
// import {Product} from "./product.model";
// import {AddProductDto} from "./addProductDto";
// import * as _ from 'lodash'
//
// @Injectable()
// export class ProductsService {
//
//     private products: Product[] = [];
//
//     insertProduct(requestJSON : AddProductDto) : string{
//
//             const prodId : string = this.randomInt().toString();
//             const newProduct = new Product(prodId, requestJSON.title, requestJSON.description, requestJSON.price);
//             this.products.push(newProduct);
//             return `Product added with ID: ${prodId}`
//     }
//
//     getProducts() {
//         // console.log(this.products)
//         return this.products;
//         // return [...this.products];
//     }
//
//     getSingleProduct(productId : string) {
//         const product = this.findProduct(productId);
//         return {...product};
//     }
//
//     updateProduct(productId: string, productTitle: string, productDesc: string, productPrice: number) {
//         const [product, index] = this.findProduct(productId);
//         const updatedProduct = {...product};
//         if (productTitle) {
//             updatedProduct.title = productTitle;
//         }
//         if (productDesc) {
//             updatedProduct.description = productDesc;
//         }
//         if (productPrice) {
//             updatedProduct.price = productPrice;
//         }
//
//         this.products[index] = updatedProduct;
//     }
//
//
//     deleteProduct(prodId: string) {
//         // const index = this.findProduct(prodId)[1]; //index jest jako drugie pole
//         // console.log("index: "+ index)
//         _.remove(this.products, function (element) {
//             return element.id === prodId;
//         })
//     }
//
//
//     private findProduct(productId: string): [Product, number]{
//         const productIndex = this.products.findIndex((prod) => prod.id === productId);
//         const product = this.products[productIndex];
//         if (!product){
//             throw new NotFoundException('Product not found')
//         }
//         return [product, productIndex];
//     }
//
//     // private findProductOnly(productId: string): Product{
//     //     console.log(productId)
//     //     const productIndex = this.products.findIndex((prod) => prod.id === productId);
//     //     const product = this.products[productIndex];
//     //     if (!product){
//     //         throw new NotFoundException('Product not found')
//     //     }
//     //     return product;
//     // }
//
//     private randomInt = (): number => {
//         return 1 + Math.floor((999999 - 1) * Math.random())
//     };
// }
