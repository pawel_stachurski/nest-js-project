// import {BeforeCreate, Column, DataType, Model, Table} from "sequelize-typescript";
// import * as bcrypt from 'bcryptjs';
//
//
// @Table({tableName: 'user'})
// export class UserModel extends Model<UserModel> {
//     @Column({
//         type: DataType.NUMBER,
//         defaultValue: DataType.NUMBER,
//         allowNull: false,
//         autoIncrement: true,
//         primaryKey: true,
//         unique: true
//     })
//     id: number;
//
//     @Column({
//         type: DataType.STRING,
//         allowNull: false,
//         unique: false
//     })
//     firstName: string;
//
//     @Column({
//         type: DataType.STRING,
//         allowNull: false,
//         unique: false
//     })
//     lastName: string;
//
//     @Column({
//         type: DataType.STRING,
//         allowNull: false,
//         unique: true
//     })
//     email: string;
//
//     @Column({
//         type: DataType.STRING,
//         allowNull: false,
//         unique: false
//     })
//     password: string;
//
//     // @Column({
//     //     type: DataType.BOOLEAN,
//     //     allowNull: false,
//     //     unique: false,
//     //     defaultValue: true
//     // })
//     // isActive: boolean;
//     //
//     // @Column({
//     //     type: DataType.STRING,
//     //     allowNull: false,
//     //     unique: false,
//     //     defaultValue: 'user' // user / mod / admin
//     // })
//     // role: string;
//
//     @BeforeCreate
//     public static async prepareCreate(user: UserModel) {
//         // user.id = uuid() //todo
//         user.password = await bcrypt.hash(user.password, 10)
//     }
//
// }
