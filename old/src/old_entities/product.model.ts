// import {Column, DataType, Model, Table} from "sequelize-typescript";
//
//
// @Table({tableName: 'products'})
// export class ProductModel extends Model<ProductModel> {
//     @Column({
//         type: DataType.NUMBER,
//         defaultValue: DataType.NUMBER,
//         allowNull: false,
//         autoIncrement: true,
//         primaryKey: true,
//         unique: true
//     })
//     id: number;
//
//     @Column({
//         type: DataType.STRING,
//         allowNull: false,
//         unique: true
//     })
//     name: string;
//
//     @Column({
//         type: DataType.STRING,
//         defaultValue: '',
//         allowNull: true,
//         unique: false
//     })
//     description: string;
//
//     @Column({
//         type: DataType.NUMBER,
//         allowNull: false,
//         unique: false
//     })
//     price: number;
//
//     @Column({
//         type: DataType.NUMBER,
//         defaultValue: 0,
//         allowNull: true,
//         unique: false
//     })
//     quantity: number;
//
//     // @Column({
//     //     type: DataType.NUMBER,
//     //     allowNull: false,
//     //     unique: false
//     // })
//     // image: number;
// }
