const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('columns', {
    id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    order: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: "columns_order_key"
    },
    projectId: {
      type: DataTypes.UUID,
      allowNull: false,
      references: {
        model: 'projects',
        key: 'id'
      }
    },
    tasksIds: {
      type: DataTypes.ARRAY(DataTypes.UUID),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'columns',
    schema: 'public',
    timestamps: true,
    indexes: [
      {
        name: "columns_order_key",
        unique: true,
        fields: [
          { name: "order" },
        ]
      },
      {
        name: "columns_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
