var DataTypes = require("sequelize").DataTypes;
var _columns = require("./columns");
var _projects = require("./projects");
var _tasks = require("./tasks");
var _user = require("./user");

function initModels(sequelize) {
  var columns = _columns(sequelize, DataTypes);
  var projects = _projects(sequelize, DataTypes);
  var tasks = _tasks(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes);

  columns.belongsTo(projects, { as: "project", foreignKey: "projectId"});
  projects.hasMany(columns, { as: "columns", foreignKey: "projectId"});
  tasks.belongsTo(user, { as: "assignee", foreignKey: "assigneeId"});
  user.hasMany(tasks, { as: "tasks", foreignKey: "assigneeId"});

  return {
    columns,
    projects,
    tasks,
    user,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
