'use strict';

/**
 * Actions summary:
 *
 * createTable "user", deps: []
 * changeColumn "tasks"."assigneeId"
 *
 **/

module.exports = {
    up: async (queryInterface, Sequelize) => {

        await queryInterface.createTable(
            'user',
            {
                "id": {
                    "type": Sequelize.UUID,
                    "field": "id",
                    "primaryKey": true,
                    "allowNull": false
                },
                "firstName": {
                    "type": Sequelize.STRING,
                    "field": "firstName",
                    "allowNull": false
                },
                "lastName": {
                    "type": Sequelize.STRING,
                    "field": "lastName",
                    "allowNull": false,
                },
                "email": {
                    "type": Sequelize.STRING,
                    "field": "email",
                    "allowNull": false,
                    "unique": true
                },
                "password": {
                    "type": Sequelize.STRING,
                    "field": "password",
                    "allowNull": false,
                },
                "isActive": {
                    "type": Sequelize.BOOLEAN,
                    "field": "isActive",
                    "allowNull": false,
                    "defaultValue": true
                },
                "role": {
                    "type": Sequelize.STRING,
                    "field": "role",
                    "allowNull": false,
                    "defaultValue": "user"
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            }
        )
        return await queryInterface.changeColumn(
            "tasks",
            "assigneeId",
            {
                "type": Sequelize.UUID,
                "field": "assigneeId",
                "allowNull": true,
                "references": {
                    "model": 'user',
                    "key": 'id'
                }
            }
        )
    },

    down: async (queryInterface, Sequelize) => {

        return await queryInterface.renameTable('user', 'old_user')
    }
};
