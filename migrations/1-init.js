'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "projects", deps: []
 * createTable "columns", deps: []
 * createTable "tasks", deps: []
 *
 **/

var info = {
    "revision": 1,
    "name": "kanban",
    "created": "2021-04-17T10:10:32.751Z",
    "comment": ""
};

var migrationCommands = [
    {
        fn: "createTable",
        params: [
            "projects",
            {
                "id": {
                    "type": Sequelize.UUID,
                    "field": "id",
                    "primaryKey": true,
                    "allowNull": false
                },
                "name": {
                    "type": Sequelize.STRING,
                    "field": "name",
                    "allowNull": false
                },
                "tasksIds": {
                    "type": Sequelize.ARRAY(Sequelize.UUID),
                    "field": "tasksIds",
                    "allowNull": true
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            }
        ]
    },
    {
        fn: "createTable",
        params: [
            "columns",
            {
                "id": {
                    "type": Sequelize.UUID,
                    "field": "id",
                    "primaryKey": true,
                    "allowNull": false
                },
                "name": {
                    "type": Sequelize.STRING,
                    "field": "name",
                    "allowNull": false
                },
                "order": {
                    "type": Sequelize.INTEGER,
                    "field": "order",
                    "allowNull": true,
                    "autoIncrement": true,
                },
                "projectId": {
                    "type": Sequelize.UUID,
                    "field": "projectId",
                    "allowNull": true,
                    "references": {
                        "model": 'projects',
                        "key": 'id'
                    }
                },
                "tasksIds": {
                    "type": Sequelize.ARRAY(Sequelize.UUID),
                    "field": "tasksIds",
                    "allowNull": true
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            }
        ]
    },
    {
        fn: "createTable",
        params: [
            "tasks",
            {
                "id": {
                    "type": Sequelize.UUID,
                    "field": "id",
                    "primaryKey": true,
                    "allowNull": false
                },
                "title": {
                    "type": Sequelize.STRING,
                    "field": "title",
                    "allowNull": false
                },
                description: {
                    "type": Sequelize.STRING,
                    "field": "description",
                    "allowNull": true
                },
                "order": {
                    "type": Sequelize.INTEGER,
                    "field": "order",
                    "allowNull": true,
                    "autoIncrement": true,
                },
                "dueDate": {
                    "type": Sequelize.DATE,
                    "field": "dueDate",
                    "allowNull": true
                },
                "isComplete": {
                    "type": Sequelize.BOOLEAN,
                    "field": "isComplete",
                    "allowNull": false,
                    "defaultValue": false
                },
                "assigneeId": {
                    "type": Sequelize.UUID,
                    "field": "assigneeId",
                    "allowNull": true
                },
                "createdAt": {
                    "type": Sequelize.DATE,
                    "field": "createdAt",
                    "allowNull": false
                },
                "updatedAt": {
                    "type": Sequelize.DATE,
                    "field": "updatedAt",
                    "allowNull": false
                }
            }
        ]
    },
];

module.exports = {
    pos: 0,
    up: function (queryInterface, Sequelize) {
        var index = this.pos;
        return new Promise(function (resolve, reject) {
            function next() {
                if (index < migrationCommands.length) {
                    let command = migrationCommands[index];
                    console.log("[#" + index + "] execute: " + command.fn);
                    index++;
                    queryInterface[command.fn].apply(queryInterface, command.params).then(next, reject);
                } else
                    resolve();
            }

            next();
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.renameTable('projects', 'old_projects')
        await queryInterface.renameTable('columns', 'old_columns')
        return await queryInterface.renameTable('tasks', 'old_tasks')
    },
    info: info
};
