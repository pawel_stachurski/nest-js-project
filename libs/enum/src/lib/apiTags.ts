export enum ApiTags {
    users = 'Users and auth Controller',
    projects = 'Projects Controller',
    columns = 'Columns Controller',
    tasks = 'Tasks Controller'
}
