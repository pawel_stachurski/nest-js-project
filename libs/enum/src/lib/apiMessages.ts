export enum ApiMessages {
    unauthorized = 'Unauthorized Access',
    columnExists = 'Column with given name already exists in this project',
    emailInUse = 'This email address is already in use',
    userNotFound = 'User with given email not found',
    invalidCredentials = 'Given login or password is incorrect',
    projectNotFound = 'Project with given ID not found',
    taskNotFound = 'Task with given ID not found',
}
