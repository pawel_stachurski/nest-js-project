export enum ApiPaths {
    tasks='tasks',
    columns='columns',
    projects='projects',
    getAll='getAll',
    create='create',
    users='users',
    auth='auth',
    login='login',
    register='register',
    assignUser = 'assignUser',
    getUserTasks = 'getUserTasks'
}
