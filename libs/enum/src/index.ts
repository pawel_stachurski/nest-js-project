import {ApiPaths} from "./lib/apiPaths";
import {ApiTags} from "./lib/apiTags";
import {ApiMessages} from "./lib/apiMessages";

export const apiPathsEnum = ApiPaths;
export const apiTagsEnum = ApiTags;
export const apiMessagesEnum = ApiMessages
